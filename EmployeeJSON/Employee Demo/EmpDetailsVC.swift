//
//  EmpDetailsVC.swift
//  Employee Demo
//
//  Created by Parinay on 13/02/21.
//

import UIKit

class EmpDetailsVC: UIViewController {
    
    var id: String = ""
    
    
    @IBOutlet weak var nameLable: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    
    @IBOutlet weak var salaryLabel: UILabel!
    
    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    @IBOutlet weak var idLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Employee Details"
        print(id)
        getMethod()
        // Do any additional setup after loading the view.
    }
    
    func getMethod() {
        
        guard let url = URL(string: "http://localhost:3000/posts/"+id) else {
            print("Error: cannot create URL")
            return
        }
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            let decoder = JSONDecoder()

            do {
                let people = try decoder.decode(employee.self, from: data)
                DispatchQueue.main.async {
                    self.idLabel.text = people.id
                    self.nameLable.text = people.name
                    self.salaryLabel.text = people.salary
                    self.statusLabel.text = people.maritalStatus
                    self.emailLabel.text = people.email
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }

}
