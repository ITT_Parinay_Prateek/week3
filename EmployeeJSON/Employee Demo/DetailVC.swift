//
//  DetailVC.swift
//  Employee Demo
//
//  Created by Parinay on 12/02/21.
//

import UIKit

class DetailVC: UIViewController {
    
    var emp = employee(id: "", name: "", email: "", password: "", maritalStatus: "", salary: "")
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    @IBOutlet weak var emailLabel: UILabel!
    
    
    @IBOutlet weak var salaryLabel: UILabel!
    
    
    @IBOutlet weak var statusLabel: UILabel!
    
    
    @IBOutlet weak var idLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Details"
        self.emailLabel.text = emp.email
        self.nameLabel.text = emp.name
        self.salaryLabel.text = emp.salary
        self.statusLabel.text = emp.maritalStatus
        self.idLabel.text = emp.id
        
    }
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { [self] (action) in
            DispatchQueue.main.async {
                deleteMethod()
            }
            navigationController?.popViewController(animated: true)

            dismiss(animated: true, completion: nil)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func deleteMethod() {
        guard let url = URL(string: "http://localhost:3000/posts/"+emp.id) else {
            print("Error: cannot create URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling DELETE")
                print(error!)
                return
            }
            guard data != nil else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            /*do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON")
                    return
                }
                guard let prettyJsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted) else {
                    print("Error: Cannot convert JSON object to Pretty JSON data")
                    return
                }
                guard String(data: prettyJsonData, encoding: .utf8) != nil else {
                    print("Error: Could print JSON in String")
                    return
                }
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }*/
        }.resume()
        
        
    }
}
