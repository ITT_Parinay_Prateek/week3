//
//  empdetails.swift
//  Employee Demo
//
//  Created by Raunak Sinha on 12/02/21.
//

import Foundation

struct empdetail : Codable {
    var id: Int
    var name: String
    var email: String
    var password: String
    var maritalStatus : String
    var salary : String
}
