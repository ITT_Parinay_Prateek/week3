//
//  ViewController.swift
//  Employee Demo
//
//  Created by Parinay on 11/02/21.
//

import UIKit

class ViewController: UIViewController {
    
    var list: [employee] = []
    var curr = employee(id: "", name: "", email: "", password: "", maritalStatus: "", salary: "")
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var PassTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToHideKeyboardOnTapOnView()
        self.getMethod()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        list = []
        emailTextField.text = ""
        PassTextField.text=""
    }

    
    @IBAction func LoginButtonTapped(_ sender: Any) {
        var isemp = false
        DispatchQueue.main.async { [self] in
            getMethod()
        }
        if emailTextField.text == nil {
            err()
            return
        }
        if PassTextField.text == nil {
            err()
            return
        }
        
        let email = emailTextField.text
        let pass = PassTextField.text
        
        
        for l in list {
            print(l)
            if l.email == email! {
                if l.password == pass!{
                    curr = l
                    isemp = true
                }
            }
        }
        if !isemp {
            err()
            return
        }
        
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    
    func err() {
        
        let alert = UIAlertController(title: "ERROR", message: "Wrong Details", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            
            alert.dismiss(animated: true, completion: nil)
            
        }
        
        alert.addAction(okAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    func getMethod() {
        
        guard let url = URL(string: "http://localhost:3000/posts/") else {
            print("Error: cannot create URL")
            return
        }
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            let decoder = JSONDecoder()

            do {
                let people = try decoder.decode([employee].self, from: data)
                self.list = people
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let destinationVC = segue.destination as? DetailVC
            destinationVC?.emp = curr
            
        }
    }
}


extension UIViewController
{
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
