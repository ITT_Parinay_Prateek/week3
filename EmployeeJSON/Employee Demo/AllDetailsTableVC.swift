//
//  AllDetailsTableVC.swift
//  Employee Demo
//
//  Created by Parinay on 12/02/21.
//

import UIKit

class AllDetailsTableVC: UITableViewController {
    
    var list: [employee] = []
    var selectedItem = employee(id: "", name: "", email: "", password: "", maritalStatus: "", salary: "")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        
        title = "All Employees"
        
        self.getMethod()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tableView.setEditing(false, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedItem = list[indexPath.row]
        
        performSegue(withIdentifier: "detailSegue", sender: nil)
        
    }
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let toDoItem = list[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "employeeName")!
        
        DispatchQueue.main.async {
            cell.textLabel?.text = toDoItem.name
        }
        
        return cell
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }
    
    func getMethod() {
        
        guard let url = URL(string: "http://localhost:3000/posts/") else {
            print("Error: cannot create URL")
            return
        }
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            let decoder = JSONDecoder()

            do {
                let people = try decoder.decode([employee].self, from: data)
                self.list = people
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            let destinationVC = segue.destination as? EmpDetailsVC
            destinationVC?.id = selectedItem.id
        }
    }
    
}
