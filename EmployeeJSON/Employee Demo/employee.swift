//
//  model.swift
//  Employee Demo
//
//  Created by Parinay on 11/02/21.
//

import Foundation

struct employee : Codable {
    var id: String
    var name: String
    var email: String
    var password: String
    var maritalStatus : String
    var salary : String
}
