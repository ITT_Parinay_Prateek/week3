//
//  SignupViewController.swift
//  Employee Demo
//
//  Created by Parinay on 11/02/21.
//

import UIKit

class SignupViewController: UIViewController{
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var salaryTextField: UITextField!
    
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    @IBOutlet weak var statusTextView: UITextField!
    
    let status = ["Single" , "married", "single", "Married"]
    
    var s : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupToHideKeyboardOnTapOnView()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        
        if emailTextField.text != "" {
            if passwordTextField.text != "" {
                if nameTextField.text != "" {
                    if salaryTextField.text != "" {
                        if statusTextView.text == status[0] || statusTextView.text == status[1] || statusTextView.text == status[2] || statusTextView.text == status[3]{
                            let emp = employee(id: String(UUID().uuidString.suffix(4)), name: nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, maritalStatus: statusTextView.text!, salary: salaryTextField.text!)
                            s=emp.id
                            guard let url = URL(string: "http://localhost:3000/posts/") else {
                                        print("Error: cannot create URL")
                                        return
                                    }
                            guard let jsonData = try? JSONEncoder().encode(emp) else {
                                        print("Error: Trying to convert model to JSON data")
                                        return
                                    }
                            var request = URLRequest(url: url)
                                    request.httpMethod = "POST"
                                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                            request.setValue("application/json", forHTTPHeaderField: "Accept")
                            request.httpBody = jsonData
                            URLSession.shared.dataTask(with: request) { data, response, error in
                                       guard error == nil else {
                                           print("Error: error calling POST")
                                           print(error!)
                                           return
                                       }
                                       guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                                           print("Error: HTTP request failed")
                                           return
                                       }
                                   }.resume()
                        }else {
                            err()
                        }
                    }else{
                        err()
                    }
                }else{
                    err()
                }
            }else {
                err()
            }
        }else {
            err()
        }
        performSegue(withIdentifier: "detailsegue", sender: nil)
    }
    
    
    func err() {
        let dialogMessage = UIAlertController(title: "ERROR", message: "Wrong Details", preferredStyle: .alert)
        
         let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
             
          })
         
         dialogMessage.addAction(ok)
         self.present(dialogMessage, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsegue" {
            let destinationVC = segue.destination as? DetailVC
            destinationVC?.emp.email = emailTextField.text!
            destinationVC?.emp.name = nameTextField.text!
            destinationVC?.emp.password = passwordTextField.text!
            destinationVC?.emp.salary = salaryTextField.text!
            destinationVC?.emp.maritalStatus = statusTextView.text!
            destinationVC?.emp.id = s
            
        }
    }
}
